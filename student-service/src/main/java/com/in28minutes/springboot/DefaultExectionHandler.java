package com.in28minutes.springboot;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class DefaultExectionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ErrorMessage> SomethingWentWrong(Exception exception, WebRequest webrequest) {
		ErrorMessage exceptionResponse = new ErrorMessage(exception.getMessage(), "What else youwant to add?");
		
		return new ResponseEntity<ErrorMessage>(exceptionResponse, new  HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

class ErrorMessage {
	private String message;
	private String detail;
	
	public ErrorMessage(String message, String detail) {
		super();
		this.message = message;
		this.detail = detail;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
}
